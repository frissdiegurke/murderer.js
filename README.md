# murderer.js

[![License](https://img.shields.io/gitlab/license/oleasteo/murderer.js.svg)](COPYING)
![Version](https://img.shields.io/badge/version-0.2.0-yellowgreen.svg)

Node.js server for a hybrid social interaction game

## What is this?

This is the server for a social interaction game that is supposed to last a few days. Groups of ~15 to ~200 participants are recommended.

It can be played without any special requisites. All handy objects within your environment might be used.

A running sample can be found [here](https://mgame.xoria.de/).

The rules can be found on the main page.

## Installation

### Prerequisite: mongodb

```bash
sudo docker volume create mongo-data
sudo docker run --name mongo -d -p 127.0.0.1:27017:27017 -v mongo-data:/data/db mongo:latest
```

Create a systemd unit file as following:

```
# /etc/systemd/system/docker.mongo.service

[Unit]
Description=Mongo Container
After=docker.service
Requires=docker.service

[Service]
Restart=always
RestartSec=5
ExecStart=/usr/bin/docker start -a mongo

[Install]
WantedBy=multi-user.target
```

Enable and start.

### Build

```bash
cp config/server{,.local}.json
```

Edit the server url in *config/server.local.json*.

Adjust the legal info in *public/static/views/\** as well.

```bash
npm i
./node_modules/.bin/grunt
```

### Configuration

```bash
cp config/mailer{,.local}.json
```

Edit *config/mailer.local.json* for emails to work.

### Start Server

```bash
node src/main.js
```

#### Systemd

```
# ~/.config/systemd/user/murdererjs.service 

[Unit]
Description=Website: mgame.xoria.de

[Service]
WorkingDirectory=/home/ole/opt/murderer.js
ExecStart=/home/ole/opt/murderer.js/start.sh

[Install]
WantedBy=default.target
```

Enable and start.

