###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'

module.exports = (grunt, config, getService) ->
  helpers =
    getService: getService

    registerTaskByPattern: (name, configName, configs, regex) ->
      tasks = Object.keys(configs).filter (task) -> regex.test task
      grunt.registerTask name, tasks.map (taskKey) -> "#{configName}:#{taskKey}"

    recursiveFilesBuilder: (name, mod, invert) ->
      dependencies = mod.dependencies || []
      if invert
        (fn) ->
          _.flatten (dependencies.map (dependency) ->
            helpers.recursiveFilesBuilder(dependency, config.modules[dependency], invert) fn
          ).concat [fn name, mod]
      else
        (fn) ->
          _.flatten [fn name, mod].concat dependencies.map (dependency)->
            helpers.recursiveFilesBuilder(dependency, config.modules[dependency], invert) fn

    findFile: (modName, fn) ->
      file = fn modName
      return file if grunt.file.exists file
      return null if !config.modules[modName].dependencies?.length
      for n in _.clone(config.modules[modName].dependencies).reverse()
        file = helpers.findFile n, fn
        return file if file?
      null

    recursiveExtend: (obj, name, fn) ->
      mod = config.modules[name]
      mod.dependencies?.forEach (dependency) -> helpers.recursiveExtend obj, dependency, fn
      _.merge obj, fn name, mod
