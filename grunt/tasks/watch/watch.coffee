###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

path = require 'path'

module.exports = (config, grunt, ignored, dirs) ->
  grunt.loadNpmTasks 'grunt-contrib-watch'

  paths = config.paths

  watch =
    options:
      spawn: false
      interrupt: false # until https://github.com/gruntjs/grunt-contrib-watch/issues/377 gets fixed
      debounceDelay: 250
    dependencies:
      files: ['package.json', 'bower.json']
      tasks: ['clean', 'build_dev']
    js:
      files: "#{paths.source}/**/*.js",
      tasks: ['copy_dev_js']
    less:
      files: "#{paths.source}/**/*.less"
      tasks: ['clean:styles', 'copy_dev_less', 'copy_dev_static', 'less_dev', 'postcss_dev']
    static:
      files: ["#{paths.source}/static/**/*"].concat(config.copy.static.map (s) -> "#{paths.source}/*/#{s}/**/*")
      tasks: ['copy_dev_static', 'htmlbuild_dev']
    htmlbuild:
    # FIXME htmlbuild_dev wouldn't refresh new/removed files since file got calculated on gruntfile-init
      files: ["#{paths.source}/*/index.html", "#{paths.source}/*/templates-static/**/*"]
      tasks: ['htmlbuild_dev']
    translations:
      files: "#{paths.source}/*/translations/**/*"
      tasks: ['clean:translations', 'genTranslations', 'htmlbuild_dev']

  watch