###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'

module.exports = (config, grunt) ->
  paths = config.paths

  getContent = (name, service) ->
    """
    angular.module("#{name}").factory("#{service}", function() { "use strict";
      return #{JSON.stringify config.locales};
    });
    """

  writeFile = (getDestination, name, service) ->
    destination = getDestination name, service
    grunt.file.write destination, getContent name, service

  grunt.registerMultiTask "genLocales", "Adds an angular-service that holds the locale-data", ->
    data = this.data
    _.each data.provides, (value) ->
      writeFile data.getDestination, value.module, value.service

  all:
    provides: config.provide.angular.locales
    getDestination: (module, service) -> "#{paths.destination.dev}/scripts/#{module}/services/#{service}.js"
