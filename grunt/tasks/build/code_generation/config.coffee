###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'

module.exports = (config, grunt) ->
  paths = config.paths

  getContent = (name, id, key, env) ->
    parts = key.split "."
    data = config.loadServerConfig parts.shift(), env
    data = (if data != null && data.hasOwnProperty k then data[k] else undefined) for k in parts
    "angular.module(\"#{name}\").constant(\"#{id}\", #{JSON.stringify data});"

  writeFile = (getDestination, name, id, key, env) ->
    destination = getDestination name, id
    grunt.file.write destination, getContent name, id, key, env

  grunt.registerMultiTask "genConfig", "Adds angular-constants to provide configuration as defined.", ->
    data = this.data
    options = this.options()
    _.each options.provide, (value) ->
      writeFile options.getDestination, value.module, value.constant, value.key, data.env

  options:
    provide: config.provide.angular.config
    getDestination: (module, id) -> "#{paths.destination.dev}/scripts/#{module}/constants/#{id}.js"
  dev:
    env: "dev"
  dist:
    env: "dist"
