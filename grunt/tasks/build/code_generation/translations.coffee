###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'
path = require 'path'

module.exports = (config, grunt, helpers) ->
  paths = config.paths

  grunt.registerMultiTask "genTranslations", "Merges translations of dependent modules", ->
    data = this.data
    readFn = _.partial readTranslationsFile, data.cwd
    _.each config.modules, (mod, name) ->
      translations = {}
      any = false
      _.each config.locales.all, (ignored, locale) ->
        files = grunt.file.expand data, data.files name, locale
        any = files.length > 0 if !any
        translations[locale] = _.merge.apply _, [{}].concat files.map readFn
      destination = "#{paths.destination.dev}/scripts/#{name}/bootstrap/translations.js"
      grunt.file.write destination, data.filePrototype name, translations if any

  getLocaleLine = (locale, translation) ->
    "  $translateProvider.translations(\"#{locale}\", #{JSON.stringify translation});"

  getLocalesString = (translations) ->
    Object.keys(config.locales.all).map((locale) -> getLocaleLine locale, translations[locale]).join grunt.util.linefeed

  readTranslationsFile = (cwd, file) ->
    filePath = "#{cwd}/#{file}"
    if grunt.file.isFile filePath
      return grunt.file.readJSON filePath if file.endsWith ".json"
      parts = path.basename(file).split "."
      ignored = parts.pop() # file-extension
      key = parts.pop()
      obj = o = {}
      o = o[p] = {} for p in parts
      o[key] = grunt.file.read filePath
      obj

  all:
    cwd: paths.source
    files: (name, locale) -> [
      "#{name}/translations/#{locale}/**/*" # files within en-us/
    ]
    filePrototype: (name, translations) ->
      """
      angular.module("#{name}").config(["$translateProvider", function($translateProvider) { "use strict";
      #{getLocalesString translations}
      }]);
      """
