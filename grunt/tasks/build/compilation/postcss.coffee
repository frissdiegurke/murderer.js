###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'
path = require 'path'

module.exports = (config, grunt, helpers) ->
  grunt.loadNpmTasks "grunt-postcss"

  paths = config.paths

  postcss =
    options:
      processors: [
        require('autoprefixer')({browsers: 'last 2 versions'})
        require('cssnano')()
      ]
    libs_dist:

      src: _(config.libraries.less).map((libs) ->
        _.map libs, (lib) -> "#{paths.destination.dist}/styles/libs/#{lib.id}.min.css"
      ).flatten().value()

  _.each config.modules, (mod, name) ->
    devFiles = []
    distFiles = []
    any = false
    for ignored, keys of config.styles
      for key in keys when grunt.file.exists "#{paths.source}/#{name}/styles/#{key}.less"
        devFiles.push "#{paths.destination.dev}/styles/#{name}/#{key}.css"
        distFiles.push "#{paths.destination.dist}/styles/#{name}-#{key}.min.css"
        any = true
    if any
#      postcss[name + '_dev'] = # FIXME postcss destroys map annotation path
#        src: devFiles
#        options:
#          map:
#            inline: false
#            annotation: path.join paths.destination.dev, 'styles', name
      postcss[name + '_dist'] =
        src: distFiles

  helpers.registerTaskByPattern 'postcss_dev', 'postcss', postcss, /_dev/
  helpers.registerTaskByPattern 'postcss_dist', 'postcss', postcss, /_dist/

  postcss