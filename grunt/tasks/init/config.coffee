###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'
path = require 'path'
crypto = require 'crypto'

module.exports = (config, grunt, ignored, dirs) ->
  dbName = config.package.name.toLowerCase().replace(/\W/g, "")

  generateSecret = -> crypto.randomBytes(64).toString 'base64'
  getTaskName = (key) -> "init#{key[0].toUpperCase() + key.substring(1)}Config"

  append =
    paths:
      "dev.frontend": config.paths.destination.dev
      "dist.frontend": config.paths.destination.dist
    database:
      dev: "mongodb://localhost/#{dbName}_dev"
      dist: "mongodb://localhost/#{dbName}"
    security:
      "dev.secret": generateSecret()
      "dist.secret": generateSecret()
    modules:
      _noEnv: true
      all: _.keys config.modules
      admin: "admin"

  tasks = _.map append, (values, file) ->
    taskName = getTaskName file
    grunt.registerTask taskName, "Applies grunt-config and package.json info to server-config (#{file}).", ->
      filename = path.join dirs.main, config.paths.serverConfig, "#{file}.local.json"
      grunt.file.write filename, '{}' if !grunt.file.exists filename
      content = grunt.file.readJSON filename
      _.each values, (value, key) ->
        c = content
        parts = key.split '.'
        lastIdx = parts.length - 1
        for part, i in parts when i < lastIdx
          if c.hasOwnProperty part then c = c[part] else c = c[part] = {}
        c[parts[lastIdx]] = value
      grunt.file.write filename, JSON.stringify content, null, '  '
    taskName

  grunt.registerTask "initConfig", tasks

  {}