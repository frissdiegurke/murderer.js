/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("closed").directive("contract", function () {
  "use strict";

  var PROJECT = {project: "murderer.js", game: "M&ouml;rderspiel"};
  var PRINT_CLASS = "print-layout", COMPACT_CLASS = "compact-layout";

  return {
    restrict: "A",
    scope: {
      contract: "=",
      printLayout: "=",
      collapseOverwrite: "=",
      onSuccess: "&"
    },
    templateUrl: "/templates/closed/contract.html",
    controller: "contractCtrl",
    link: function ($scope, $element) {
      $scope.project = PROJECT;
      $scope.$watch("printLayout", function (value) {
        if (value) {
          $element.removeClass(COMPACT_CLASS).addClass(PRINT_CLASS);
        } else {
          $element.removeClass(PRINT_CLASS).addClass(COMPACT_CLASS);
        }
      });
      $scope.$watch("::contract", function (contract) {
        if (contract != null) { $scope.alias = contract.details.alias || contract.game.alias; }
      });
    }
  };
});
