/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("closed").controller("killCtrl", function (TOKEN_SIZE, $scope, $uibModalInstance, game, ringIndex) {
  "use strict";

  var REGEX_HEX = /^[a-fA-F0-9]+$/i;

  /*===================================================== Scope  =====================================================*/

  $scope.tokenLength = TOKEN_SIZE * 2; // two hex-chars per byte
  $scope.data = {token: null, message: null};
  $scope.game = game;
  $scope.ringIdx = ringIndex;

  $scope.dismiss = $uibModalInstance.dismiss;
  $scope.confirm = confirm;
  $scope.isHex = function (value) { return REGEX_HEX.test(value); };

  /*=================================================== Functions  ===================================================*/

  function confirm() {
    $scope.data.token = $scope.data.token.toUpperCase();
    $uibModalInstance.close($scope.data);
  }

});
