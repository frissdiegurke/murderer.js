/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("closed").controller("joinGameCtrl", function ($scope, $uibModalInstance, game, socket) {
  "use strict";

  /*===================================================== Scope  =====================================================*/

  $scope.groups = null;

  $scope.data = {name: null, message: null, groupId: null};

  $scope.dismiss = $uibModalInstance.dismiss;
  $scope.confirm = confirm;

  /*=============================================== Initial Execution  ===============================================*/

  /*------------------------------------------------ Fetch all groups ------------------------------------------------*/

  socket
      .query("groups:populate", _.map(game.groups, "group"))
      .then(function (groups) {
        $scope.groups = groups;
        if (groups.length === 1) { $scope.data.groupId = groups[0]._id; }
      });

  /*=================================================== Functions  ===================================================*/

  function confirm() { $uibModalInstance.close($scope.data); }

});
