/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("closed").controller("gameCtrl", function ($scope, gameId, games, modals, closedGames) {
  "use strict";

  /*===================================================== Scope  =====================================================*/

  $scope.game = null;
  $scope.murders = null;

  $scope.loading = true;
  $scope.stateIcon = games.stateIcon;

  $scope.news = $scope.score = $scope.stats = _.noop; // TODO implement
  $scope.showRingNews = _.noop; // TODO implement
  $scope.showRingTable = function (idx) { modals.gameTable($scope.game, idx, 0); };
  $scope.table = function () { modals.gameTable($scope.game, null, 2); };

  $scope.join = function () { closedGames.join($scope.game); };
  $scope.leave = function () { closedGames.leave($scope.game); };
  $scope.suicide = function () { closedGames.suicide($scope.game); };

  /*=============================================== Initial Execution  ===============================================*/

  refreshGame();

  /*=================================================== Functions  ===================================================*/

  function refreshGame() {
    $scope.loading = true;
    games
        .byId(gameId)
        .then(function (result) {
          $scope.game = games.prepareGameSingleView(result.game);
          $scope.murders = result.murders;
          $scope.loading = false;
        });
  }

});
