/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("closed").factory("closedModals", function ($uibModal) {
  "use strict";

  var Q_METHOD_REGEX = /^q[A-Z]/;

  /*==================================================== Exports  ====================================================*/

  var service = {
    joinGame: joinGame,
    suicide: suicide,
    kill: kill
  };

  qIfy(service);

  /*=================================================== Functions  ===================================================*/

  function joinGame(game) {
    return $uibModal.open({
      templateUrl: "/templates/closed/modals/join_game.html",
      controller: "joinGameCtrl",
      resolve: {game: game}
    });
  }

  function suicide(game) {
    return $uibModal.open({
      templateUrl: "/templates/closed/modals/suicide.html",
      controller: "suicideCtrl",
      resolve: {game: game}
    });
  }

  function kill(game, ringIndex) {
    return $uibModal.open({
      templateUrl: "/templates/closed/modals/kill.html",
      controller: "killCtrl",
      resolve: {game: game, ringIndex: ringIndex}
    });
  }

  function qIfy(obj) {
    _.each(_.keys(obj), function (key) {
      if (Q_METHOD_REGEX.test(key)) { return; }
      var value = obj[key];
      obj["q" + key[0].toUpperCase() + key.substring(1)] = function () { return value.apply(obj, arguments).result; };
    });
  }

  /*===================================================== Return =====================================================*/

  return service;
});
