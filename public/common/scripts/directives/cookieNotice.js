/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").directive("cookieNotice", function (cookiesAccepted) {
  "use strict";

  var EXPIRE_YEARS = 5;

  /*==================================================== Exports  ====================================================*/

  return {
    restrict: "C",
    templateUrl: "/templates/common/cookie_notice.html",
    link: link
  };

  /*=================================================== Functions  ===================================================*/

  function acceptCookies() {
    var expire = new Date();
    expire.setFullYear(expire.getFullYear() + EXPIRE_YEARS);
    cookiesAccepted.accept(expire);
  }

  function rejectCookies() { cookiesAccepted.reject(); }

  /*------------------------------------------------------ Link ------------------------------------------------------*/

  function link($scope, $element) {
    if (cookiesAccepted.init() === true) { return $element.remove(); }
    $scope.acceptCookies = function () {
      acceptCookies();
      $element.remove();
    };
    $scope.rejectCookies = function () {
      rejectCookies();
      $element.remove();
    };
  }

});
