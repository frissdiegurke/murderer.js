/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").directive("moment", function ($rootScope, $translate) {
  "use strict";

  var bodyTarget = {}; // just some new object for identity-checks

  /*==================================================== Exports  ====================================================*/

  return {
    restrict: "A",
    scope: {
      value: "=moment",
      target: "@momentTarget",
      placeholder: "@momentPlaceholder",
      format: "@momentFormat"
    },
    link: link
  };

  /*=================================================== Functions  ===================================================*/

  function fillTarget($element, target, value) {
    if (target === bodyTarget) {
      $element.text(value == null ? "" : value);
    } else if (target) {
      if (value == null) {
        $element.removeAttr(target);
      } else {
        $element.attr(target, value);
      }
    }
  }

  function update($scope, $element) {
    var value = moment($scope.value || null);
    fillTarget(
        $element,
        $scope.target || bodyTarget,
        value.isValid() ?
            value.format($scope.format || "lll") :
            $scope.placeholder ?
                $translate.instant("moment.placeholder." + $scope.placeholder) :
                ""
    );
  }

  /*------------------------------------------------------ Link ------------------------------------------------------*/

  function link($scope, $element) {
    var destroyCb = $rootScope.$on("language.update", function () { update($scope, $element); });

    $scope.$watch("target", function (newTarget, oldTarget) {
      if (oldTarget != null) { fillTarget($element, oldTarget, null); }
      update($scope, $element);
    });

    $scope.$watchGroup(["placeholder", "format", "value"], function () { update($scope, $element); });

    $scope.$on("$destroy", destroyCb);
  }

});
