/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").factory("localStorage", function ($window, cookieStorage, cookiesAccepted) {
  "use strict";

  var localStorage = $window.localStorage;

  /*==================================================== Exports  ====================================================*/

  var service;

  if (localStorage == null || typeof localStorage.getItem !== "function") {

    /*---------------------------------------- Export cookie storage fallback ----------------------------------------*/

    service = cookieStorage;

  } else {

    /*----------------------------------------- Export local storage methods -----------------------------------------*/

    service = {
      get: getItem,
      getObject: getItemJSON,
      getAll: function () { return localStorage; },
      put: cookiesAccepted.delay(setItem),
      putObject: cookiesAccepted.delay(setItemJSON),
      remove: removeItem
    };

  }

  return service;

  /*=================================================== Functions  ===================================================*/

  function getItem() { return localStorage.getItem.apply(localStorage, arguments); }

  function getItemJSON() {
    try {
      return JSON.parse(localStorage.getItem.apply(localStorage, arguments));
    } catch (e) {
      return void 0;
    }
  }

  function removeItem() { return localStorage.removeItem.apply(localStorage, arguments); }

  function setItem(key, value) { return localStorage.setItem(key, value); }

  function setItemJSON(key, value) { return localStorage.setItem(key, JSON.stringify(value)); }

});
