/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").factory("news", function (socket) {
  "use strict";

  var ICONS = {
    suicide: "fa-bomb",
    kill: "fa-spoon"
  };

  var fetchAllPromise = null, murderPromises = {};

  /*==================================================== Exports  ====================================================*/

  var service = {
    list: [],
    games: {},

    getAll: function () { return fetchAllPromise = fetchAllPromise || fetchAll(); },
    getMurders: function (gameId) { return murderPromises[gameId] = murderPromises[gameId] || fetchMurders(gameId); }
  };

  /*------------------------------------------------- Socket watcher -------------------------------------------------*/

  //socket.on("news:update.game", updateGameNews);
  //socket.on("news:update.global", updateGlobalNews);

  /*----------------------------------------------------- Return -----------------------------------------------------*/

  return service;

  /*=================================================== Functions  ===================================================*/

  function fetchAll() {
    return socket
        .query("murder:all")
        .then(function (result) {
          _.each(result.games, function (game) { service.games[game._id] = game; });
          _.each(result.murders, prepareMurder);
          Array.prototype.splice.apply(service.list, [0, service.list.length].concat(result.murders));
          return service.list;
        });
  }

  function prepareMurder(murder) {
    murder.isMurder = true;
    murder.game = service.games[murder.game];
    if (murder.ring != null) { murder.ringIdx = _.indexOf(murder.game.rings, murder.ring); }
    murder.victim = findUserData(murder.game, murder.victim);
    murder.murderer = findUserData(murder.game, murder.murderer);
    murder.suicide = murder.victim == null;
    murder.type = murder.suicide ? "suicide" : "kill";
    murder.icon = ICONS[murder.type];
    return murder;
  }

  function findUserData(game, userId) {
    if (userId == null) { return userId; }
    var userData = null;
    _.some(game.groups, function (groupData) {
      return _.some(groupData.users, function (uD) { if (uD.user === userId) { return userData = uD; } });
    });
    return userData;
  }

  function fetchMurders(gameId) { return socket.query("murder:byGame", gameId); }

});
