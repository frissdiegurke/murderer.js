/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").factory("modals", function ($uibModal) {
  "use strict";

  var Q_METHOD_REGEX = /^q[A-Z]/;

  /*==================================================== Exports  ====================================================*/

  var service = {
    gameTable: gameTable
  };

  qIfy(service);

  /*=================================================== Functions  ===================================================*/

  function gameTable(game, ringIndex, ringsAround) {
    return $uibModal.open({
      templateUrl: "/templates/common/modals/game_table.html",
      controller: "gameTableCtrl",
      size: ringsAround ? "full" : "lg",
      resolve: {game: game, ringIndex: ringIndex, ringsAround: ringsAround}
    });
  }

  function qIfy(obj) {
    _.each(_.keys(obj), function (key) {
      if (Q_METHOD_REGEX.test(key)) { return; }
      var value = obj[key];
      obj["q" + key[0].toUpperCase() + key.substring(1)] = function () { return value.apply(obj, arguments).result; };
    });
  }

  /*===================================================== Return =====================================================*/

  return service;
});
