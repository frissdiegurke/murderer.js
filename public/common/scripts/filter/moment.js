/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").filter("moment", function ($translate) {
  "use strict";
  // to be used with care since it cannot update on locale change

  var DEFAULT_FORMAT = "lll";
  var DEFAULT_PLACEHOLDER = "";

  return function (value, options) {
    value = moment(value || null);
    if (value.isValid()) {
      if (typeof options === "string") {
        return value.format(options);
      }
      if (typeof options === "object" && options !== null && options.format.length) {
        return value.format(options.format);
      }
      return value.format(DEFAULT_FORMAT);
    }
    if (typeof options === "object" && options !== null && options.placeholder.length) {
      return $translate.instant("moment.placeholder." + options.placeholder);
    }
    return DEFAULT_PLACEHOLDER;
  };
});
