/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").controller("navigationCtrl", function ($scope, $location) {
  "use strict";

  /*===================================================== Scope  =====================================================*/

  $scope.checkRoute = checkRoute;
  $scope.checkRoutePrefix = checkRoutePrefix;

  /*=================================================== Functions  ===================================================*/

  function checkRoute(value, url) {
    if (url == null) { url = $location.url(); }
    if (value instanceof Array) { return _.some(value, checkRoute, url); }
    return url === value;
  }

  function checkRoutePrefix(value, url) {
    if (url == null) { url = $location.url(); }
    if (value instanceof Array) { return _.some(value, checkRoutePrefix, url); }
    return url === value || url.startsWith(value + "/");
  }

});
