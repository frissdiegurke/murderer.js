/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").controller("gameTableCtrl", function ($scope, $uibModalInstance, socket, games, news, game, ringIndex, ringsAround) {
  "use strict";

  var RINGS_AROUND = ringsAround, MAX_RINGS_VISIBLE = 2 * RINGS_AROUND + 1;

  var highlightActive = ringIndex != null;
  var currentRingIndex = ringIndex || 0;

  $scope.firstRing = null;
  $scope.lastRing = null;
  $scope.rings = null;
  $scope.hoveredCol = null;
  $scope.activeCol = null;
  $scope.hoveredMurder = null;

  $scope.game = game;
  $scope.compact = true;
  $scope.allRings = game.rings.length <= MAX_RINGS_VISIBLE;
  $scope.participants = [];
  $scope.users = {};
  $scope.murders = {};
  $scope.killed = {};

  $scope.dismiss = $uibModalInstance.dismiss;
  $scope.prevRings = function () { setRingIndex(--currentRingIndex); };
  $scope.nextRings = function () { setRingIndex(++currentRingIndex); };
  $scope.hoverCol = function (index) { $scope.hoveredCol = index; };
  $scope.leaveCol = function () { $scope.hoveredCol = null; };
  $scope.hoverMurder = function (murder) { $scope.hoveredMurder = murder._id; };
  $scope.leaveMurder = function () { $scope.hoveredMurder = null; };

  /*=============================================== Initial Execution  ===============================================*/

  prepareUsers();
  _.each(game.rings, prepareRing);
  setRingIndex(currentRingIndex);
  news.getMurders(game._id).then(prepareMurders);

  /*=================================================== Functions  ===================================================*/

  function setRingIndex(value) {
    var rings = game.rings.length;
    if (highlightActive) {
      if (value < 0) { value = 0; }
      if (value >= rings) { value = rings - 1; }
      $scope.firstRing = value === 0;
      $scope.lastRing = value === rings - 1;
    } else {
      if (value < RINGS_AROUND) { value = RINGS_AROUND; }
      if (value >= rings - RINGS_AROUND) { value = rings - RINGS_AROUND - 1; }
      if (value < 0) { value = 0; }
      $scope.firstRing = value <= RINGS_AROUND;
      $scope.lastRing = value >= rings - RINGS_AROUND - 1;
    }
    if ($scope.allRings || value <= RINGS_AROUND) { // visible rings start at 0
      $scope.rings = game.rings.slice(0, MAX_RINGS_VISIBLE);
      if (highlightActive) { $scope.activeCol = value; }
    } else if (value >= rings - RINGS_AROUND - 1) { // visible rings end at last and do not start at 0
      $scope.rings = game.rings.slice(rings - MAX_RINGS_VISIBLE);
      if (highlightActive) { $scope.activeCol = value - (rings - MAX_RINGS_VISIBLE); }
    } else { // somewhere in between, no extremal ring visible
      $scope.rings = game.rings.slice(value - RINGS_AROUND, value + RINGS_AROUND + 1);
      if (highlightActive) { $scope.activeCol = RINGS_AROUND; }
    }
    currentRingIndex = value;
  }

  function prepareUsers() {
    _.each(game.groups, function (groupData) {
      _.each(groupData.users, function (userData) {
        $scope.participants.push($scope.users[userData.user] = {
          _id: userData.user,
          name: userData.name,
          isSelf: userData.user === socket.identity._id,
          group: groupData.group
        });
      });
    });
  }

  function prepareMurders(murders) {
    _.each(murders, function (murder) {
      if (murder.ring == null) {
        _.each(game.rings, function (ring) { prepareMurder(_.extend({suicide: true}, murder, {ring: ring._id})); });
      } else {
        prepareMurder(_.clone(murder));
      }
    });
  }

  function prepareMurder(murder) {
    var murders = $scope.murders;
    if (!murders.hasOwnProperty(murder.murderer)) { murders[murder.murderer] = {}; }
    var userMurders = murders[murder.murderer];
    if (!userMurders.hasOwnProperty(murder.ring)) { userMurders[murder.ring] = []; }
    userMurders[murder.ring].push(murder);
    murder.murderer = $scope.users[murder.murderer];
    if (murder.victim != null) {
      var killed = $scope.killed;
      if (!killed.hasOwnProperty(murder.victim)) { killed[murder.victim] = {}; }
      killed[murder.victim][murder.ring] = murder;
      murder.victim = $scope.users[murder.victim];
    }
    murder.plain = {
      victim: murder.victim && murder.victim.name,
      murderer: murder.murderer.name,
      date: murder.cdate,
      message: murder.message
    };
  }

  function prepareRing(ring, idx) { ring.index = idx; }

});
