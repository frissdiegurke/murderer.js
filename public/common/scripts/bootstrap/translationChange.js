/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Events:
 *   * $rootScope - language.update - The locale in use has been updated.
 */
angular.module("common").run(function (DEFAULT_LOCALE, $rootScope, $translate, locales) {
  "use strict";

  /*===================================================== Scope  =====================================================*/

  $rootScope.language = null;

  $rootScope.setLanguage = function (language) { $translate.use(language); };

  /*------------------------------------------------- Scope Watcher  -------------------------------------------------*/

  $rootScope.$on("$translateChangeSuccess", refreshScope);

  /*=============================================== Initial Execution  ===============================================*/

  if (!locales.all.hasOwnProperty($translate.use())) { $translate.use(DEFAULT_LOCALE); } else { refreshScope(); }

  /*=================================================== Functions  ===================================================*/

  function refreshScope() {
    var language = $rootScope.language = $translate.use();
    moment.locale(language);
    $rootScope.$broadcast("language.update", language);
  }

});
