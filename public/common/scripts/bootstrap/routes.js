/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").config(function ($routeProvider, $locationProvider) {
  "use strict";

  /*================================================= HTML5 Routing  =================================================*/

  $locationProvider.html5Mode(true);

  /*===================================================== Routes =====================================================*/

  /*------------------------------------------------------ Home ------------------------------------------------------*/

  // @ngInject
  $routeProvider
      .when("/", {
        templateUrl: "/views/common/home.html"
      })
  ;

  /*----------------------------------------------- Legal Information  -----------------------------------------------*/

  // @ngInject
  $routeProvider
      .when("/cookie-policy", {
        templateUrl: "/views/cookiePolicy.html"
      })

      .when("/privacy-policy", {
        templateUrl: "/views/privacyPolicy.html"
      })

      .when("/legal-info", {
        templateUrl: "/views/legalInfo.html"
      })
  ;

  /*----------------------------------------------------- Games  -----------------------------------------------------*/

  // @ngInject
  $routeProvider
      .when("/games", {
        templateUrl: "/views/common/games.html",
        controller: "gamesCtrl" // to be implemented in extending modules
      })
      .when("/games/:gameId", {
        templateUrl: "/views/common/game.html",
        controller: "gameCtrl", // to be implemented in extending modules
        resolve: {gameId: function ($route) { return $route.current.params.gameId; }}
      })
  ;

  /*------------------------------------------------------ News ------------------------------------------------------*/

  // @ngInject
  $routeProvider
      .when("/news", {
        templateUrl: "/views/common/news.html",
        controller: "newsCtrl" // to be implemented in extending modules
      })
  ;

  /*-------------------------------------------------- Error Pages  --------------------------------------------------*/

  // @ngInject
  $routeProvider
      .otherwise({
        templateUrl: "/views/common/404.html"
      })
  ;

});
