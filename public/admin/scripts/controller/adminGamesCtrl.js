/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("admin").controller("adminGamesCtrl", function ($scope, games, adminGames) {
  "use strict";

  /*===================================================== Scope  =====================================================*/

  $scope.groups = null;
  $scope.groupsList = null;

  $scope.games = [];
  $scope.displayedGames = [];
  $scope.stateIcon = games.stateIcon;

  $scope.sort = {
    state: games.sortValue.state
  };
  $scope.createGame = createGame;
  $scope.lockGame = lockGame;
  $scope.startGame = startGame;
  $scope.resumeGame = resumeGame;
  $scope.pauseGame = pauseGame;
  $scope.stopGame = stopGame;
  $scope.removeGame = removeGame;

  /*=============================================== Initial Execution  ===============================================*/

  adminGames.all()
      .then(function (games) {
        Array.prototype.push.apply($scope.games, _.each(games, adminGames.prepareGameListView));
        return $scope.games;
      });

  /*=================================================== Functions  ===================================================*/

  function lockGame(game) {
    adminGames.lock(game).then(function (g) { adminGames.prepareGameListView(_.extend(game, g)); });
  }

  function startGame(game) {
    adminGames.start(game).then(function (g) { adminGames.prepareGameListView(_.extend(game, g)); });
  }

  function resumeGame(game) {
    adminGames.resume(game).then(function (g) { adminGames.prepareGameListView(_.extend(game, g)); });
  }

  function pauseGame(game) {
    adminGames.pause(game).then(function (g) { adminGames.prepareGameListView(_.extend(game, g)); });
  }

  function stopGame(game) {
    adminGames.stop(game).then(function (g) { adminGames.prepareGameListView(_.extend(game, g)); });
  }

  function createGame() {
    adminGames.create().then(function (g) { $scope.games.unshift(adminGames.prepareGameListView(g)); });
  }

  function removeGame(game) {
    adminGames.remove(game).then(function () { $scope.games.splice(_.indexOf($scope.games, game), 1); });
  }

});
