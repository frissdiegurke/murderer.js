/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("admin").config(function ($routeProvider) {
  "use strict";

  /*===================================================== Routes =====================================================*/

  /*------------------------------------------------------ ACP  ------------------------------------------------------*/

  // @ngInject
  $routeProvider
      .when("/acp", {
        templateUrl: "/views/admin/acp.html"
      })

      .when("/acp/games", {
        templateUrl: "/views/admin/games.html",
        controller: "adminGamesCtrl"
      })

      .when("/acp/groups", {
        templateUrl: "/views/admin/groups.html",
        controller: "groupsCtrl"
      })

      .when("/acp/users", {
        templateUrl: "/views/admin/users.html",
        controller: "usersCtrl"
      })
  ;

});
