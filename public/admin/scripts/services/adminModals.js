/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("admin").factory("adminModals", function ($rootScope, $uibModal) {
  "use strict";

  var Q_METHOD_REGEX = /^q[A-Z]/;

  /*==================================================== Exports  ====================================================*/

  var service = {
    markdownPreview: markdownPreview,
    createGame: createGame
  };

  qIfy(service);

  /*=================================================== Functions  ===================================================*/

  function markdownPreview(text) {
    return $uibModal.open({
      templateUrl: "/templates/admin/modals/markdown_preview.html",
      controller: "markdownPreviewCtrl",
      size: "lg",
      resolve: {plainText: _.constant(text)}
    });
  }

  function createGame() {
    return $uibModal.open({
      templateUrl: "/templates/admin/modals/create_game.html",
      controller: "createGameCtrl",
      size: "lg",
      backdrop: "static",
      keyboard: false
    });
  }

  function qIfy(obj) {
    _.each(_.keys(obj), function (key) {
      if (Q_METHOD_REGEX.test(key)) { return; }
      var value = obj[key];
      obj["q" + key[0].toUpperCase() + key.substring(1)] = function () { return value.apply(obj, arguments).result; };
    });
  }

  /*===================================================== Return =====================================================*/

  return service;
});
