/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("admin").factory("adminGames", function (socket, adminModals) {
  "use strict";

  /*==================================================== Exports  ====================================================*/

  //noinspection UnnecessaryLocalVariableJS
  var service = {
    all: function () { return socket.query("games:admin.all"); },

    create: create,
    lock: function (game) { return socket.query("game:lock", game._id); }, // TODO confirm modal
    start: function (game) { return socket.query("game:start", game._id); }, // TODO confirm modal
    resume: function (game) { return socket.query("game:resume", game._id); },
    pause: function (game) { return socket.query("game:pause", game._id); },
    stop: function (game) { return socket.query("game:stop", game._id); }, // TODO confirm modal
    remove: function (game) { return socket.query("game:remove", game._id); }, // TODO confirm modal

    prepareGameListView: prepareGameListView
  };

  /*=================================================== Functions  ===================================================*/

  function prepareGameListView(game) {
    game.participants = _.sum(_.map(game.groups, function (g) { return g.users.length; }));
    game.state = game.ended ? "stopped" : game.active ? "running" : game.started ? "paused" : "initial";
    return game;
  }

  function create() {
    return adminModals.qCreateGame().then(function (data) { return socket.query("game:create", data); });
  }

  /*===================================================== Return =====================================================*/

  return service;
});
