###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'
path = require 'path'

if typeof String.prototype.endsWith != "function"
  String.prototype.endsWith = (string) -> this.lastIndexOf(string) == this.length - string.length
  String.prototype.startsWith = (string) -> this.indexOf(string) == 0

PATH_BASE = path.join __dirname, 'grunt'

dirs =
  main: __dirname
  base: PATH_BASE
  config: path.join PATH_BASE, 'config'
  services: path.join PATH_BASE, 'services'
  tasks: path.join PATH_BASE, 'tasks'

module.exports = (grunt) ->
  ###--------------------------------------- Initialize Settings and Helpers  ---------------------------------------###
  getService = (name) -> require path.join dirs.services, name
  config = getService('loadConfig') grunt, __dirname, dirs.config
  helpers = getService('helpers') grunt, config, getService

  ###----------------------------------------------- Initialize Tasks -----------------------------------------------###
  loadTask = (file) -> require(path.join dirs.tasks, file) config, grunt, helpers, dirs

  # TODO jshint validation within dist

  grunt.initConfig cfg =
    pkg: config.package
  # source file generation
    initConfig: loadTask "init/config"
  # file removal
    clean: loadTask "clean/clean"
  # dependencies
    #npminstall: loadTask "dependencies/npminstall"
    bower: loadTask "dependencies/bower"
  # file management
    copy: loadTask "build/copy"
  # file generation
    genTranslations: loadTask "build/code_generation/translations"
    genLocales: loadTask "build/code_generation/locales"
    genConstants: loadTask "build/code_generation/constants"
    genConfig: loadTask "build/code_generation/config"
  # html build
    htmlbuild: loadTask "build/compilation/htmlbuild"
  # styles build
    less: loadTask "build/compilation/less"
    postcss: loadTask "build/compilation/postcss"
  # scripts build
    ngAnnotate: loadTask "build/compilation/ngannotate"
    uglify: loadTask "build/compilation/uglify"
  # file observation
    watch: loadTask "watch/watch"

  ###------------------------------------------------ Define Aliases ------------------------------------------------###
  grunt.registerTask 'default', ['dist']
  # common tasks
  grunt.registerTask 'init', Object.keys(cfg).filter (task) -> /^init[A-Z]/.test task
  grunt.registerTask 'dev', ['build_dev', 'watch']
  grunt.registerTask 'dev_deps', ['dependencies', 'build_dev', 'watch']
  grunt.registerTask 'dist', ['dependencies', 'build_dist']
  # - dependencies
  grunt.registerTask 'dependencies', ['bower']
  # - build process
  grunt.registerTask 'build_dev', ['copy_dev', 'generate_dev', 'compile_dev']
  grunt.registerTask 'build_dist', ['copy_dev', 'generate_dist', 'compile_dist']
  # - - code-generation
  grunt.registerTask 'generate_dev', _.map Object.keys(cfg).filter((task) -> /^gen[A-Za-z]+(?:_dev)?$/.test task),
    (k) -> if cfg[k].hasOwnProperty "dev" then k + ":dev" else k
  grunt.registerTask 'generate_dist', _.map Object.keys(cfg).filter((task) -> /^gen[A-Za-z]+(?:_dist)?$/.test task),
    (k) -> if cfg[k].hasOwnProperty "dist" then k + ":dist" else k
  # - - compilation
  grunt.registerTask 'compile_dev', ['less_dev', 'postcss_dev', 'htmlbuild_dev']
  grunt.registerTask 'compile_dist', ['ngAnnotate', 'uglify', 'less_dist', 'postcss_dist', 'copy_dist', 'htmlbuild_dist']
