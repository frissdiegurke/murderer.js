#!/bin/bash

BASE_DIR="$(cd "$(dirname "$0")" && pwd)"

. ~/.nvm/nvm.sh
nvm use default
(cd "$BASE_DIR" && node src/main.js)

