/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var bunyan = require("bunyan");

var bus = require.main.require("./utils/bus").main;
var userC = require.main.require("./core/user/controller");

var connections;

connections = {};

/*------------------------------------------------- Create / Update  -------------------------------------------------*/

function createConnection(user, ip) {
  return connections[user._id] = {
    sockets: {},
    user: user,
    ip: ip,
    emit: function (messageId, data) {
      _.each(this.sockets, function (socket) {
        socket.emit(messageId, data);
      });
    },
    log: bunyan.logger.socket.child({user: user})
  };
}

module.exports.updateUser = function (user) {
  if (_.has(connections, user._id)) {
    connections[user._id].user = user;
  }
};

module.exports.add = function (user, ip, sock) {
  var conn;
  if (_.has(connections, user._id)) {
    conn = connections[user._id];
    conn.user = user;
    conn.ip = ip;
  } else {
    conn = createConnection(user, ip);
  }
  conn.sockets[sock.id] = sock;
  conn.log.info({socket: sock}, "associated socket");
  return conn;
};

/*---------------------------------------------------- Get / Find ----------------------------------------------------*/

module.exports.all = connections;

module.exports.bySocket = function (socket) {
  return _.find(connections, function (conn) {
    return _.has(conn.sockets, socket.id);
  });
};

module.exports.findByModule = function (name) {
  return _.filter(connections, function (conn) {
    return userC.belongsToModule(conn.user, name);
  });
};

module.exports.findByModulePermission = function (name) {
  return _.filter(connections, function (conn) {
    return userC.isModulePermitted(conn.user, name);
  });
};

/*------------------------------------------------------ Remove ------------------------------------------------------*/

function removeUser(id) {
  connections[id].log.end();
  delete connections[id];
}

module.exports.removeUser = function (userId) {
  if (_.has(connections, userId)) {
    removeUser(userId);
  }
};

module.exports.remove = function (socket) {
  var conn;
  conn = module.exports.bySocket(socket);
  if (conn != null) {
    delete conn.sockets[socket.id];
    if (_.isEmpty(conn.sockets)) {
      removeUser(conn.user._id);
    }
  }
};

bus.on("socket:disconnected", function (socket) {
  module.exports.remove(socket);
});
