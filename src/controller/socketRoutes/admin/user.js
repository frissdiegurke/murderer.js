/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var Q = require("q");

var userC = require.main.require("./core/user/controller");

module.exports = function (queryRoute) {
  var projection = {username: 1, admin: 1, email: 1};

  queryRoute("users:activated", function () { return userC.qFind(this, {activated: true}, projection); });
  queryRoute("users:not-activated", function () { return userC.qFind(this, {activated: false}, projection); });

  queryRoute("user:remove", function (data) {
    return userC.qFindById(this, data).then(function (user) { return Q.ninvoke(user, "remove"); });
  });

  queryRoute("user:update", function (data) { return userC.qUpdateById(this, data._id, _.omit(data, ["_id"])); });
};
