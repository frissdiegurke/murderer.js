/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var groupC = require.main.require("./core/group/controller");

module.exports = function (queryRoute) {
  queryRoute("groups:all", function (data) { return groupC.qFind(this, data); });

  queryRoute("group:remove", function (data) { return groupC.qRemoveById(this, data); });

  queryRoute("group:create", function (data) { return groupC.qCreate(this, data); });

  queryRoute("group:update", function (data) {
    return groupC.qFindByIdAndUpdate(this, {_id: data._id}, data, {new: true});
  });
};
