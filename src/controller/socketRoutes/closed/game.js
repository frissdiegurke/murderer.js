/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var Q = require("q");

var gameC = require.main.require("./core/game/controller");

module.exports = function (queryRoute) {
  queryRoute("games:joined", function () { return gameC.qFindJoined(this); });

  queryRoute("game:join", function (data) {
    if (typeof data !== "object" || data == null || typeof data.gameId !== "string" || typeof data.name !== "string" || !data.name.length || typeof data.message !== "string" || !data.message.length || typeof data.groupId !== "string") {
      return Q.reject("Bad request.");
    }
    return gameC.qJoin(this, data.gameId, data.name, data.message, data.groupId);
  });

  queryRoute("game:leave", function (data) {
    if (typeof data !== "string") { return Q.reject("Bad request."); }
    return gameC.qLeave(this, data);
  });
};
