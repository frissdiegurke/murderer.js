/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var crypto = require("crypto");
var bcrypt = require("bcrypt");

var config = require("./config").main;

module.exports.encryptPassword = function (password) {
  var pwConfig = config.security.password;
  return bcrypt.hashSync(password, bcrypt.genSaltSync(pwConfig && pwConfig.hashStrength));
};

module.exports.checkPassword = function (candidate, hash) {
  return bcrypt.compareSync(candidate, hash);
};

module.exports.generateToken = function (bytes) {
  return crypto.randomBytes(bytes || config.security.token.bytes).toString("hex");
};

module.exports.generateRandom = function (bytes, encoding) {
  return crypto.randomBytes(bytes).toString(encoding || "hex");
};

module.exports.md5 = function (str) {
  return crypto.createHash("md5").update(str).digest("hex");
};
