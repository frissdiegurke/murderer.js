/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");

module.exports = function (string, data) {
  if (typeof string !== "string") { return ""; }
  if (data == null) { return string; }

  var check = function (val, key) {
    if (typeof val === "function") {
      string = string.replace(new RegExp("<%\\s*" + key + "(\\([^)]*\\))\\s*%>", "gi"), val);
    } else {
      string = string.replace(new RegExp("<%\\s*" + key + "\\s*%>", "gi"), val);
      if (typeof val === "object") {
        _.each(val, function (v, k) { check(v, key + "." + k); });
      }
    }
  };

  _.each(data, check);

  return string;
};
