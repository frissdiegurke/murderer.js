/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var RingBuffer = require.main.require("./utils/bunyan/RingBuffer");

module.exports = function (ignored, log) {
  process.on("SIGINT", function () {
    log.info("Received SIGINT.");
    require.main.exports.exit(130);
  });

  process.on("SIGUSR2", function () {
    log.info("received SIGUSR2. flushing all logging-buffers");
    RingBuffer.flushAll({trigger: "SIGUSR2"});
  });
};
