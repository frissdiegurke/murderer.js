/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var express = require("express");

var config = require.main.require("./utils/config").main;

var ROBOTS_TXT_DEV = "User-agent: *\nDisallow: /";
var ROBOTS_TXT_PROD = "User-agent: *\nAllow:";

module.exports = function (app) {
  var router = express.Router();

  router.get("/robots.txt", _.partial(sendConstant, getRobotsTxt()));

  app.use(router);
};

function sendConstant(str, ignored, res) { res.end(str); }

function getRobotsTxt() { return config.server.development ? ROBOTS_TXT_DEV : ROBOTS_TXT_PROD; }
