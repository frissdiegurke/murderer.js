/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var Q = require("q");

var controller = require("../controller");

/*===================================================== Exports  =====================================================*/

exports.findJoined = findJoined;
exports.join = join;
exports.leave = leave;

/*==================================================== Functions  ====================================================*/

function findJoined(scope) {
  return controller
      .qFind(scope, {"groups.users.user": scope.user._id}, {_id: 1})
      .then(function (games) { return _.map(games, "_id"); });
}

function getJoinErrorOrGroupIndex(groups, groupId, userId, name) {
  var err = null, groupIdx = null;
  var groupCheck = function (group, i) {
    if (group.group.toString() === groupId) { groupIdx = i; }
    return _.some(group.users, function (u) {
      return err = u.user === userId ? "Already joined." : u.name === name ? "Name already in use." : null;
    });
  };
  if (_.some(groups, groupCheck)) { return Q.reject(err); }
  if (groupIdx == null) { return Q.reject("Group not found."); }
  return groupIdx;
}

function join(scope, gameId, name, message, groupId) {
  var userId = scope.user._id;
  return controller
      .qFindById(scope, gameId)
      .then(function (game) {
        if (game.started) { return Q.reject("Game already locked."); }
        return getJoinErrorOrGroupIndex(game.groups, groupId, userId, name);
      })
      .then(function (groupIdx) {
        var push = {};
        push["groups." + groupIdx + ".users"] = {user: userId, name: name, message: message};
        scope.log.info({gameId: gameId, name: name, groupIdx: groupIdx}, "user joins game");
        return controller.qUpdateById(scope, gameId, {$push: push});
      })
      .then(function () { return controller.qGroupDetails(scope, gameId); });
}


function leave(scope, gameId) {
  var userId = scope.user._id;
  return controller
      .qUpdate(scope,
          {_id: gameId, started: false, "groups.users.user": userId},
          {$pull: {"groups.$.users": {user: userId}}})
      .then(function () { scope.log.info({gameId: gameId}, "user left game"); })
      .then(function () { return controller.qGroupDetails(scope, gameId); });
}
