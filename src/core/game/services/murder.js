/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var Q = require("q");
var userC = require.main.require("./core/user/controller");
var ringC = require.main.require("./core/ring/controller");
var murderC = require.main.require("./core/murder/controller");
var config = require.main.require("./utils/config").main;

var controller = require("../controller");

/*===================================================== Exports  =====================================================*/

exports.byKill = kill;
exports.bySuicide = suicide;
exports.getIndexIfSuicideCommittable = getIndexIfSuicideCommittable;

/*==================================================== Functions  ====================================================*/

/*--------------------------------------------- relevant for kills only  ---------------------------------------------*/

function kill(scope, ip, userId, gameId, ringId, token, message, triggered) {
  return findGameData(scope, gameId, {
    ip: ip,
    ringId: ringId,
    murderer: userId,
    token: token,
    message: message,
    triggered: triggered,
    suicide: false
  })
      .then(findCrimeScene)
      .then(createMurder)
      .then(applyMurderToRing)
      .then(broadcast);
}

function findCrimeScene(data) {
  var rings = data.game.rings, ringIdx = _.findIndex(rings, function (ring) { return ring._id.equals(data.ringId); });
  if (!~ringIdx) { return Q.reject("Ring not found."); }
  var victimData = findVictimData(data.ring = rings[data.ringIndex = ringIdx], data.murderer, data.token);
  return typeof victimData === "string" ? Q.reject(victimData) : _.extend(data, victimData);
}

function findVictimData(ring, murderer, token) {
  var lastAliveIdx = _.findLastIndex(ring.chain, function (entry) { return entry.murder == null; });
  if (!~lastAliveIdx) { return "Not alive."; }
  var lastAliveIsMurderer = ring.chain[lastAliveIdx].user.equals(murderer);
  var skippedVictims = [];

  var chain = ring.chain, current;
  for (var i = 0; i < chain.length; i++) {
    current = chain[i];
    if (current.vulnerable) {
      if (lastAliveIsMurderer) {
        if (token === current.token) {
          // victim found and token valid
          return {index: i, victim: current.user, skippedVictims: skippedVictims};
        } else if (current.murder == null) {
          // victim found but tokens don't match
          return "Token invalid.";
        } else {
          // keep track of entries that would've been valid victims (since they committed suicide)
          skippedVictims.push(i);
        }
      } else if (current.murder == null) {
        // item is next potential murderer
        lastAliveIsMurderer = chain[lastAliveIdx = i].user.equals(murderer);
      }
    }
  }

  return lastAliveIsMurderer ? "No victim found." : "Not alive.";
}

function applyMurderToRing(data) {
  var $set = {};
  _.each(data.skippedVictims, function (i) { $set["chain." + i + ".vulnerable"] = false; });
  $set["chain." + data.index + ".vulnerable"] = false;
  $set["chain." + data.index + ".murder"] = data.murder._id;
  return ringC
      .qUpdateById(data.scope, data.ring._id, {$set: $set, $inc: {active: -(1 + data.skippedVictims.length)}})
      .then(_.constant(data))
      .fail(function (err) {
        data.scope.log.error({err: err}, "failed to update ring with murder");
        data.scope.log.info("attempt to revert murder");
        return murderC
            .qRemoveById(data.scope, data.murder._id)
            .then(function () { data.scope.log.info("revert successful"); },
                function (err) { data.scope.log.fatal({err: err, data: data}, "revert failed"); })
            .then(_.constant(Q.reject("Internal error.")));
      });
}

/*-------------------------------------------- relevant for suicides only --------------------------------------------*/

function suicide(scope, ip, userId, gameId, message, triggered) {
  return findGameData(scope, gameId, {
    ip: ip,
    murderer: userId,
    message: message,
    triggered: triggered,
    suicide: true
  })
      .then(findSuicideIndices)
      .then(createMurder)
      .then(applySuicideToRing)
      .then(broadcast);
}

function findSuicideIndices(data) {
  var rings = data.game.rings;
  data.suicides = _.compact(_.map(rings, function (ring, ringIdx) {
    var idx = getIndexIfSuicideCommittable(data.murderer, ring);
    if (~idx) { return {ring: ring, index: idx, ringIndex: ringIdx}; }
  }));
  if (!data.suicides.length) { return Q.reject("Not alive in any non-resolved ring."); }
  return data;
}

function getIndexIfSuicideCommittable(userId, ring) {
  // check whether any other user is still alive and find index
  var anyOther = false, found = false, chain = ring.chain, current, idx = -1;
  for (var i = 0; i < chain.length; i++) {
    current = chain[i];
    if (current.murder == null) {
      if (found) { return idx; }
      if (current.user.equals(userId)) {
        if (anyOther) { return i; }
        idx = i;
        found = true;
      } else {
        anyOther = true;
      }
    }
  }
  return -1;
}

function applySuicideToRing(data) {
  return Q
      .all(_.map(data.suicides, function (suicide) {
        var $set = {};
        $set["chain." + suicide.index + ".murder"] = data.murder._id;
        return ringC
            .qUpdateById(data.scope, suicide.ring._id, {$set: $set, $inc: {active: -1}})
            .fail(function (err) {
              data.scope.log.fatal({err: err}, "failed to update ring with suicide");
              return Q.reject("Internal error.");
            })
            .then(function () { return getSuicideEmailData(data.game, suicide); });
      }))
      .then(function (emailData) {
        data.scope.log.info("suicide executed");
        notifySuicideHunters(data.scope, data.game, emailData).done();
        return data;
      });
}

function getSuicideEmailData(game, suicide) {
  var chain = suicide.ring.chain, prevIdx = suicide.index, nextIdx = suicide.index;
  var last = chain.length - 1;
  do {
    prevIdx--;
    if (prevIdx < 0) { prevIdx = last; }
  } while (prevIdx !== suicide.index && chain[prevIdx].murder != null);
  if (prevIdx === suicide.index) { return; } // last remaining user in ring => no notification needed
  do {
    nextIdx++;
    if (nextIdx > last) { nextIdx = 0; }
  } while (nextIdx !== prevIdx && chain[nextIdx].murder != null);
  var target = null, user = chain[nextIdx].user;
  _.some(game.groups, function (groupData) {
    return target = _.find(groupData.users, function (userData) { return userData.user.equals(user); });
  });
  if (target == null) {
    throw new Error("Target not identified.");
  }
  // TODO add suicide-message of user into email
  return {
    last: prevIdx === nextIdx,
    addressee: chain[prevIdx].user,
    target: target.name,
    ringIndex: suicide.ringIndex,
    ring: suicide.ring
  };
}

function notifySuicideHunters(scope, game, emailData) {
  // send grouped emails to each ex-hunter of whom who committed suicide
  var addressee = _.groupBy(emailData, "addressee");
  return Q.all(_.map(addressee, function (emailD, adr) {
    return userC
        .qFindById(scope, adr)
        .then(function (user) {
          var survived = {}, nextTarget = {};
          _.each(emailD, function (eD) { (eD.last ? survived : nextTarget)[eD.ringIndex] = eD.target; });
          return Q
              .all([
                sendSuicideEmail(scope, user, game, survived, true),
                sendSuicideEmail(scope, user, game, nextTarget, false)
              ])
              .fail(function (err) {
                scope.log.warn({err: err, addressee: user}, "failed to notify hunter");
              });
        });
  }));
}

function sendSuicideEmail(scope, addressee, game, target, survived) {
  if (!_.keys(target).length) { return; }
  var listing = _.map(target, function (name, idx) {
    idx++; // Since we use 1-based indices for end-user
    return survived ? "#" + idx : "  #" + idx + ": " + name;
  }).join(survived ? ", " : "\n  ");
  return userC.qSendMailByKey(scope, addressee, "game." + (survived ? "survived" : "newMissions"), {
    listing: listing,
    link: config.server.url + "contracts",
    game: game.name
  });
}

/*------------------------------------------- shared by kills and suicides -------------------------------------------*/

function findGameData(scope, gameId, data) {
  return controller
      .qFindById(scope, gameId)
      .then(function (game) {
        return _.extend(data, {
          game: game,
          scope: _.extend({}, scope, {log: scope.log.child(data)})
        });
      })
      .then(populateRings);
}

function populateRings(data) {
  if (!data.suicide && !data.game.active) { throw new Error("Game is not active."); }
  return controller
      .qPopulate(data.scope, data.game, "rings")
      .fail(function (err) {
        data.scope.log.error({err: err}, "failed to populate game rings");
        return Q.reject("Internal error.");
      })
      .then(_.constant(data));
}

function createMurder(data) {
  data.scope.log = data.scope.log.child(_.omit(data, ["scope"]));
  data.scope.log.info("valid " + (data.suicide ? "suicide" : "kill"));
  return murderC
      .qCreate(data.scope, {
        ip: data.ip,
        message: data.message,
        murderer: data.murderer,
        victim: data.victim,
        ring: data.suicide ? null : data.ring._id,
        game: data.game._id,
        trigger: data.triggered ? data.scope.user._id : null
      })
      .then(function (murder) {
        data.scope.log = data.scope.log.child({murder: data.murder});
        return _.extend(data, {murder: murder});
      })
      .fail(function (err) {
        data.scope.log.error({err: err}, "failed to create murder entry");
        return Q.reject("Internal error.");
      });
}

function broadcast(data) {
  murderC.qNewsBroadcast(data.scope, data.murder).done();
  return data;
}
