/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var Q = require("q");

var model = require("../model");

/*===================================================== Exports  =====================================================*/

exports.activeContract = findActiveContract;
exports.activeContracts = findActiveContracts;

/*==================================================== Functions  ====================================================*/

function findActiveContracts(scope) {
  var userId = scope.user._id;
  var query = model
      .find({started: true, ended: false, "groups.users.user": userId},
          {name: 1, active: 1, groups: 1, rings: 1, "schedule.end": 1})
      .populate("rings groups.group");
  return Q
      .nbind(query.exec, query)()
      .then(function (games) {
        return _.map(games, function (game) {
          var usersData = getUsersData(game, userId);
          return {
            _id: game._id,
            name: game.name,
            multiGroup: game.groups.length > 1,
            alias: usersData.alias,
            schedule: {end: game.schedule.end},
            active: game.active,
            contracts: _.map(game.rings, _.partial(getContractDataOfRing, userId, usersData.map))
          };
        });
      });
}

function findActiveContract(scope, gameId, ringId) {
  var userId = scope.user._id;
  var query = model
      .findOne({_id: gameId, started: true, ended: false, "groups.users.user": userId},
          {name: 1, active: 1, groups: 1, rings: 1, "schedule.end": 1})
      .populate("rings groups.group");
  return Q
      .nbind(query.exec, query)()
      .then(function (game) {
        var usersData = getUsersData(game, userId);
        var ring = _.find(game.rings, function (ring) { return ring._id.equals(ringId); });
        if (ring != null) { return getContractDataOfRing(userId, usersData.map, ring); }
      });
}

/**
 * Determines the status of given user within given ring.
 *
 * Possible resolutions:
 *   * User is alive and has a contract. (result.target != null && result.token != null)
 *   * User is alive and sole survivor. (result.survived)
 *   * User is not alive and ring is resolved with survivor. (result.resolved && result.survivor != null)
 *   * User is not alive and ring is resolved without survivor. (result.resolved && result.survivor == null)
 *   * User is not alive and ring is not resolved. (!result.resolved)
 *
 * @param {ObjectID} userId The ID of the user to get contract data of.
 * @param {Object} usersMap Maps User._id to the user-data defined within Game.groups[].users[].
 * @param {Object} ring The Ring instance.
 * @returns {{ringId: ObjectID}} The result may also contain the following properties:
 *            * present - true iff the user is present within the ring.
 *            * dead - true iff the user is dead within the ring.
 *            * token - String iff the user might get killed within the ring.
 *            * survived - true iff the user is the last survivor within the ring.
 *            * target - Object iff the user has an active contract within the ring.
 *            * resolved - true iff the ring got resolved.
 *            * survivor - Object iff the user is not alive and the ring got resolved.
 */
function getContractDataOfRing(userId, usersMap, ring) {
  var result = {ringId: ring._id};
  var chain = ring.chain, aliveChain = [], current, idx, nextAliveIdx;
  for (var i = 0, j = 0; i < chain.length; i++) {
    current = chain[i];
    if (current.murder == null) {
      aliveChain.push(current);
      j++;
    }
    if (current.user.equals(userId)) {
      result.present = true;
      result.dead = current.murder != null;
      if (!result.dead) {
        result.token = current.token;
        nextAliveIdx = j;
      }
      idx = i;
    }
  }
  if (result.present && !result.dead) {
    if (aliveChain.length === 1) {
      result.resolved = true;
      result.survived = true;
      delete result.token;
    } else {
      var nextAliveEntry = nextAliveIdx === aliveChain.length ? aliveChain[0] : aliveChain[nextAliveIdx];
      result.target = getUserData(usersMap, nextAliveEntry);
    }
  } else if (aliveChain.length <= 1) {
    result.resolved = true;
    result.survivor = aliveChain.length ? getUserData(usersMap, aliveChain[0]) : null;
  }
  return result;
}

function getUsersData(game, userId) {
  var alias = null;
  var searchAlias = true;
  var usersMap = {};
  _.each(game.groups, function (groupData) {
    _.each(groupData.users, function (userData) {
      usersMap[userData.user] = userData._doc;
      if (searchAlias && userData.user.equals(userId)) {
        alias = getUserData(usersMap, userData);
        alias.group = groupData.group;
        delete alias.message;
        searchAlias = false;
      }
    });
  });
  if (searchAlias) { throw new Error("User not found."); }
  return {map: usersMap, alias: alias};
}

function getUserData(usersMap, chainEntry) {
  var userData = _.clone(usersMap[chainEntry.user]);
  userData._id = userData.user;
  delete userData.user;
  return userData;
}
