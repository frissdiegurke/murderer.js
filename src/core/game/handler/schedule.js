/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");

var bus = require.main.require("./utils/bus").main;
var serverScope = require.main.require("./utils/serverScope");

var controller = require("../controller");

/*
 * The interval is needed for two reasons:
 *  1. The maximum lifetime of setTimeout is ~25d.
 *  2. setTimeout might get imprecise if it's set too long.
 */
var INTERVAL_TIME = 3600000; // 1 hour

var checkAgain = {};
var queued = {};

/*================================================== Event Handler  ==================================================*/

bus.on("database:connected", function () {
  controller
      .qFind(serverScope, null, {started: 1, active: 1, ended: 1, "schedule.end": 1, "schedule.start": 1})
      .done(function (games) { _.each(games, checkGame); });
});

controller.post("save", checkGame);
controller.pre("remove", cleanUp);

setInterval(checkAllAgain, INTERVAL_TIME);

/*==================================================== Functions  ====================================================*/

function checkAllAgain() {
  var check = checkAgain;
  checkAgain = {};
  _.each(check, checkGame);
}

function startGame(game) { controller.qStart(serverScope, game._id).then(checkGame); }

function endGame(game) { controller.qStop(serverScope, game._id).then(); }

function queueCheck(game, delay) { queued[game] = setTimeout(function () { checkGame(game); }, delay); }

function checkGame(game) {
  cleanUp(game);
  var now = Date.now();
  if (!game.started && game.schedule.start != null && game.schedule.end.getTime() > now) {
    serverScope.log.debug({game: game}, "registering game start schedule");
    checkTime(game, game.schedule.start.getTime() - now, startGame);
  }
  if (game.started && !game.ended) {
    serverScope.log.debug({game: game}, "registering game end schedule");
    checkTime(game, game.schedule.end.getTime() - now, endGame);
  }
}

function checkTime(game, delay, cb) {
  if (delay <= 50) {
    cb(game);
  } else if (delay > INTERVAL_TIME) {
    checkAgain[game._id] = game;
  } else {
    queueCheck(game, delay);
  }
}

function cleanUp(game) {
  serverScope.log.debug({game: game}, "clean up game schedule");
  var gameId = game._id;
  if (checkAgain.hasOwnProperty(gameId)) { delete checkAgain[gameId]; }
  if (queued.hasOwnProperty(gameId)) {
    clearTimeout(queued[gameId]);
    delete queued[gameId];
  }
}
