/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var COLLECTION_NAME = "Murder";

var mongoose = require("mongoose");
var modelBase = require.main.require("./utils/modelBase");

var Schema = require("mongoose").Schema;
var ObjectID = Schema.Types.ObjectId;

/*================================================ Schema Definition  ================================================*/

var GameSchema = new Schema(
    {
      cdate: {type: Date, default: Date.now},
      ip: {type: String, required: true},
      message: String,

      murderer: {type: ObjectID, ref: "User", required: true},
      victim: {type: ObjectID, ref: "User"},

      upVotes: {type: [{type: ObjectID, ref: "User"}], default: []},

      ring: {type: ObjectID, ref: "Ring"},
      game: {type: ObjectID, ref: "Game", required: true},
      trigger: {type: ObjectID, ref: "User"} // if an admin triggers a murder he's going to be referenced here
    }
);

/*===================================================== Exports  =====================================================*/

var model = mongoose.model(COLLECTION_NAME, GameSchema);
modelBase(model, exports, null, ["murderer", "game"]);

exports.COLLECTION_NAME = COLLECTION_NAME;
