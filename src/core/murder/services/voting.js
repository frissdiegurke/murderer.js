/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var Q = require("q");

var controller = require("../controller");

var userC = require.main.require("./core/user/controller");

/*===================================================== Exports  =====================================================*/

exports.upVote = upVote;
exports.mayVote = mayVote;
exports.hasVoted = hasVoted;

/*==================================================== Functions  ====================================================*/

function upVote(scope, murderId) {
  var user = scope.user;
  return controller
      .qUpVoteData(scope, murderId)
      .then(function (murder) {
        if (murder == null) { return Q.reject("Murder not found."); }
        if (!mayVote(user, murder)) { return Q.reject("Forbidden."); }
        var votes = murder.upVotes.length;
        var query = {};
        if (hasVoted(user, murder)) {
          query.$pull = {upVotes: user._id};
          votes--;
        } else {
          query.$addToSet = {upVotes: user._id};
          votes++;
        }
        return controller
            .qUpdateById(scope, murderId, query)
            .then(function (data) {
              if (data.hasOwnProperty("nModified") && !data.nModified) { return Q.reject("Update failed."); }
              return votes;
            });
      });
}

function hasVoted(user, murder) { return _.some(murder.upVotes, function (entry) { return user._id.equals(entry); }); }

function mayVote(user, murder) {
  var userId = user._id;
  if (murder.game.ended || !userC.isModulePermitted(user, "closed") || userId.equals(murder.murderer)) { return false; }
  return _.some(murder.game.groups, function (groupData) {
    return _.some(groupData.users, function (userData) { return userId.equals(userData.user); });
  });
}
