/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var Q = require("q");

var model = require("./model");
var ctrlBase = require.main.require("./utils/controllerBase");
var connections = require.main.require("./controller/connections");

var gameC = require.main.require("./core/game/controller");

var voting = require("./services/voting");

/*===================================================== Exports  =====================================================*/

ctrlBase(model, exports);

exports.qNewsBroadcast = newsBroadcast;

exports.qNews = news;
exports.qFindByGamePopulated = findByGamePopulated;
exports.qUpVoteData = upVoteData;
exports.qUpVote = voting.upVote;

/*==================================================== Functions  ====================================================*/

function upVoteData(scope, murderId) {
  var query = model
      .findById(murderId, {ip: 0})
      .populate("game", {"groups.users.user": 1, ended: 1});
  return Q.nbind(query.exec, query)();
}

function news(scope) {
  var user = scope.user;
  var query = model
      .find(null, {ip: 0})
      .sort("-cdate")
      .populate("trigger", {username: 1, avatarUrl: 1})
      .populate("game", {"groups.users.user": 1, ended: 1})
      .limit(20); // TODO add pagination
  var gameProjection = {"groups.users.name": 1, "groups.users.user": 1, name: 1, rings: 1};
  return Q.spread([
        gameC.qFind(scope, {started: true}, gameProjection),
        Q
            .nbind(query.exec, query)()
            .then(function (murders) {
              return _.map(murders, function (murder) {
                murder = murder._doc;
                murder.hasUpVoted = checkUpVoted(user, murder);
                murder.upVotes = murder.upVotes.length;
                murder.game = murder.game._id;
                return murder;
              });
            })
      ],
      function (games, murders) { return {games: games, murders: murders}; }
  );
}

function newsBroadcast(scope, murder) {
  var query = model
      .findById(murder._id, {ip: 0})
      .populate("trigger", {username: 1, avatarUrl: 1})
      .populate("game", {"groups.users.user": 1, ended: 1});
  return Q
      .nbind(query.exec, query)()
      .then(function (murder) {
        if (murder == null) { return null; }
        var upVotes = murder.upVotes.length, gameId = murder.game._id;
        _.each(connections.findByModulePermission("common"), function (connection) {
          var user = connection.user;
          var murderClone = _.clone(murder._doc);
          murderClone.hasUpVoted = checkUpVoted(user, murderClone);
          murderClone.upVotes = upVotes;
          murderClone.game = gameId;
          connection.emit("game:kill.committed", murderClone);
        });
      });
}

function checkUpVoted(user, murder) {
  return (murder.mayUpVote = voting.mayVote(user, murder)) && voting.hasVoted(user, murder);
}

function findByGamePopulated(scope, gameId) {
  var query = model
      .find({game: gameId}, {upVotes: 0, ip: 0})
      .sort("cdate")
      .populate("trigger", {username: 1, avatarUrl: 1});
  return Q.nbind(query.exec, query)();
}
