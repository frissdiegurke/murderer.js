/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var COLLECTION_NAME = "Ring";

var mongoose = require("mongoose");
var modelBase = require.main.require("./utils/modelBase");

var Schema = require("mongoose").Schema;
var ObjectID = Schema.Types.ObjectId;

/*================================================ Schema Definition  ================================================*/

var GameSchema = new Schema(
    {
      cdate: {type: Date, default: Date.now},
      active: {type: Number},
      game: {type: ObjectID, ref: "Game", required: true},

      chain: {
        type: [{
          user: {type: ObjectID, ref: "User", required: true},
          token: {type: String, required: true},
          murder: {type: ObjectID, ref: "Murder"},
          vulnerable: {type: Boolean, default: true} // [A,B,C] Needed because if B commits suicide, A may still kill B
                                                     // (in real life the kill might have happened before the suicide).
        }], default: []
      }
    }
);

/*===================================================== Exports  =====================================================*/

var model = mongoose.model(COLLECTION_NAME, GameSchema);
modelBase(model, exports, null, ["game"]);

exports.COLLECTION_NAME = COLLECTION_NAME;
