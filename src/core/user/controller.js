/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var Q = require("q");
var model = require("./model");
var ctrlBase = require.main.require("./utils/controllerBase");

var email = require("./services/email");
var sample = require("./services/sample");
var transport = require("./services/transport");
var permission = require("./services/permission");
var emailUpdate = require("./services/emailUpdate");
var passwordUpdate = require("./services/passwordUpdate");

/*===================================================== Exports  =====================================================*/

ctrlBase(model, exports);

exports.findByModule = permission.findByModule;
exports.findByModulePermission = permission.findByModulePermission;
exports.belongsToModule = permission.belongsToModule;
exports.isModulePermitted = permission.isModulePermitted;

exports.createGuest = sample.createGuest;
exports.createAdmin = sample.createAdmin;

exports.createTransport = transport.createTransport;

exports.qSendMailByKey = email.sendTemplateByKey;

exports.qRequestPasswordToken = passwordUpdate.requestPasswordToken;
exports.qUpdatePasswordByToken = passwordUpdate.updatePasswordByToken;

exports.qUpdateEmail = emailUpdate.trigger;

exports.qRemoveSelf = function (scope) { return Q.ninvoke(scope.user, "remove"); };
exports.qUpdateSelf = qUpdateSelf;

/*==================================================== Functions  ====================================================*/

function qUpdateSelf(scope, data) {
  return exports
      .qFindByIdAndUpdate(scope, scope.user._id, _.omit(data, model.LOCKED_FIELDS), {new: true})
      .fail(function (err) {
        scope.log.error({err: err}, "user update failed");
        return Q.reject(err);
      });
}
