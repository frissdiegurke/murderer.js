/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var tmpCount = 0;

/*===================================================== Exports  =====================================================*/

exports.createGuest = createGuest;
exports.createAdmin = createAdmin;

/*==================================================== Functions  ====================================================*/

function createGuest() {
  var id = tmpCount++;
  return {
    _id: "guest/" + id,
    guest: true,
    username: "guest/" + id
  };
}

function createAdmin() {
  var id = tmpCount++;
  return {
    _id: "admin/" + id,
    admin: true,
    activated: true,
    username: "admin/" + id
  };
}
