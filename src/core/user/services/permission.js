/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var model = require("../model");

/*===================================================== Exports  =====================================================*/

exports.findByModule = findByModule;
exports.findByModulePermission = findByModulePermission;
exports.belongsToModule = belongsToModule;
exports.isModulePermitted = isModulePermitted;

/*==================================================== Functions  ====================================================*/

function findByModule(name, cb) {
  switch (name) {
    case "admin":
      return model.find({admin: true, activated: true}, cb);
    case "active":
      return model.find({admin: false, activated: true}, cb);
    case "closed":
      return model.find({activated: false}, cb);
    case "common":
    case "open":
      return cb(null, []);
  }
  cb(new Error("Module '" + name + "' unknown"));
}

function findByModulePermission(name, cb) {
  switch (name) {
    case "admin":
      return model.find({admin: true, activated: true}, cb);
    case "active":
      return model.find({activated: true}, cb);
    case "closed":
    case "common":
      return model.find({}, cb);
    case "open":
      return cb(null, []);
  }
  cb(new Error("Module '" + name + "' unknown"));
}

function belongsToModule(user, name) {
  switch (name) {
    case "admin":
      return user.admin && user.activated;
    case "active":
      return !user.admin && user.activated;
    case "closed":
      return !user.activated && !user.guest;
    case "common":
      return false;
    case "open":
      return user.guest;
  }
  throw new Error("Module '" + name + "' unknown");
}

function isModulePermitted(user, name) {
  switch (name) {
    case "admin":
      return user.admin && user.activated;
    case "active":
      return user.activated;
    case "closed":
      return !user.guest;
    case "common":
      return true;
    case "open":
      return user.guest;
  }
  throw new Error("Module '" + name + "' unknown");
}
